#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;
#define size 5
class Student {

public:
    void input()
    {
        for (int i = 0; i < size; i++)
        {
            cin >> scores[i];
        }
    }
    int calculateTotalScore() 
    {
        int sayac = 0;
        for (int i = 0; i < size; i++) {
            sayac += scores[i];
        }
        return sayac;
    }
private:
    int scores[size];
};
int main() {
    int n; 
    cin >> n;
    Student* s = new Student[n]; 

    for (int i = 0; i < n; i++) {
        s[i].input();
    }

  
    int kristen_score = s[0].calculateTotalScore();

    int count = 0;
    for (int i = 1; i < n; i++) {
        int total = s[i].calculateTotalScore();
        if (total > kristen_score) {
            count++;
        }
    }

    cout << count;

    return 0;
}
