#include <iostream>
#include <cstdio>
#define size 4
using namespace std;
int max_of_four(int A[size]);

int main() {
    int A[size];
    for (int i = 0; i < 4; i++)
    {
        cin >> A[i];
    }
    int ans = max_of_four(A);
    cout << ans;


    return 0;
}
int max_of_four(int A[size])
{
    int max = A[0];
    for (int i = 0; i < size; i++)
    {
        if (max < A[i])
        {
            max = A[i];
        }
    }
    return max;
}