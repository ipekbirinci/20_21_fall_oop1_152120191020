#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str)
{
	stringstream stringst(str);
	vector<int> sonuc;
	char c;
	int temp;

	while (stringst >> temp) 
	{
		sonuc.push_back(temp);
		stringst >> c;
	}

	return sonuc;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);

    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}