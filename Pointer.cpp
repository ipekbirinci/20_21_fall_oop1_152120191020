#include <stdio.h>
#include <iostream>
using namespace std;

void update(int* a, int* b) {
    int sum, min;
    sum = *a + *b;
    if (*a < *b)
        min = *b - *a;
    else
        min = *a - *b;
    cout << sum <<endl << min;
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    cin>> a>> b;
    update(pa, pb);
   
    return 0;
}